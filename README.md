# Description

OpenDKIM 2.10.3 on Alpine 3.13 configured for serving multiple domains.
For the first domain keys are generated if none found. 

# Build

```
podman build . -t quay.io/tbalsys/opendkim:2.10.3-r1
```

# Run

```
podman run --name opendkim \
  -e DOMAIN=balsys.eu.org \
  -v ./data:/dkimkeys \
  quay.io/tbalsys/opendkim:2.10.3-r1
```

# Environment variables

- DOMAIN: generate keys for this domain if none found
- SELECTOR: use this selector for key generation (default: mail)

# Volumes

- Mount `/dkimkeys` for key and configuration table persistence

# Ports

- OpenDKIM is listening on port `8891`
