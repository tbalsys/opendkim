#!/bin/sh

set -e

: ${DOMAIN:?}
: ${SELECTOR:=mail}

if [ ! -f /dkimkeys/trustedhosts ]; then
    printf "127.0.0.1\nlocalhost\n*.$DOMAIN\n" > /dkimkeys/trustedhosts
fi

if [ ! -f /dkimkeys/keytable ]; then
    echo $SELECTOR._domainkey.$DOMAIN $DOMAIN:$SELECTOR:/dkimkeys/$DOMAIN/$SELECTOR.private > /dkimkeys/keytable
fi

if [ ! -f /dkimkeys/signingtable ]; then
    echo "*@$DOMAIN $SELECTOR._domainkey.$DOMAIN" > /dkimkeys/signingtable
fi

for d in $(cat /dkimkeys/trustedhosts)
do
    if [ -n $d ] && [ $d != "localhost" ] && [ $d != "127.0.0.1" ]; then
        if [ ! -d "/dkimkeys/${d:2}" ]; then
            mkdir -p /dkimkeys/${d:2}
            opendkim-genkey \
                --directory=/dkimkeys/${d:2} \
                --selector=$SELECTOR \
                --domain=${d:2} \
                --nosubdomains

            echo "DKIM key generated for ${d:2}. Add the public key to the domain’s DNS records"
            cat /dkimkeys/${d:2}/$SELECTOR.txt
        fi  
    fi
done

mkdir -p /run/opendkim
chown -R opendkim:opendkim /dkimkeys /run/opendkim
chmod -R g-rwx,o-rwx /dkimkeys

exec s6-svscan /etc/services.d
