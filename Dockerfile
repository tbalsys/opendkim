FROM alpine:3.13

RUN apk  --no-cache add s6 opendkim opendkim-utils && \
  echo "*.* -/dev/stdout" >> /etc/syslog.conf

EXPOSE 8891

CMD ["entrypoint.sh"]

COPY rootfs /
